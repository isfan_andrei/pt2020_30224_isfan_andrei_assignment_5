import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Taskuri {
	private List<MonitoredData> lista = new ArrayList<MonitoredData>();
	private Map<String, Integer> activitatiTotale = new HashMap<String, Integer>();
	private Map<Integer, Map<String, Integer>> activitatiPeZi = new HashMap<Integer, Map<String, Integer>>();
	private Map<String, Long> activitateDurata = new HashMap<String, Long>();

	public void setLista(List<MonitoredData> list) {
		this.lista = list;
	}

	public void setActivitati(Map<String, Integer> activitati) {
		this.activitatiTotale = activitati;
	}

	public void setActivitatiPeZi(Map<Integer, Map<String, Integer>> activitatiPeZi) {
		this.activitatiPeZi = activitatiPeZi;
	}

	public void setActivitateDurata(Map<String, Long> activitateDurata2) {
		this.activitateDurata = activitateDurata2;
	}

	public Map<String, Integer> getActivitatiTotale() {
		return activitatiTotale;
	}

	public void setActivitatiTotale(Map<String, Integer> activitatiTotale) {
		this.activitatiTotale = activitatiTotale;
	}

	public List<MonitoredData> getLista() {
		return lista;
	}

	public Map<Integer, Map<String, Integer>> getActivitatiPeZi() {
		return activitatiPeZi;
	}

	public Map<String, Long> getActivitateDurata() {
		return activitateDurata;
	}

	public List<MonitoredData> creareLista(String file) throws IOException {
		return Files.lines(Paths.get(file)).map(s -> s.split("\t\t")).map(s -> {
			return new MonitoredData(LocalDateTime.parse(s[0].replace(" ", "T")),
					LocalDateTime.parse(s[1].replace(" ", "T")), s[2]);
		}).collect(Collectors.toList());
	}

	public int totalZile() {
		Set<String> aux = this.lista.stream().map(MonitoredData::dataString).collect(Collectors.toSet());
		return aux.size();
	}

	public Map<String, Integer> activitatiPeZiDupaNumarulLor() {
		return this.lista.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.collectingAndThen(
						Collectors.mapping(MonitoredData::getActivityLabel, Collectors.counting()), Long::intValue)));
	}

	public Map<Integer, Map<String, Integer>> activitatiInFiecareZiExistenta() {
		return this.lista.stream()
				.collect(Collectors.groupingBy(MonitoredData::getDay,
						Collectors.groupingBy(MonitoredData::getActivityLabel,
								Collectors.collectingAndThen(
										Collectors.mapping(MonitoredData::getActivityLabel, Collectors.counting()),
										Long::intValue))));
	}
	
	static SimpleDateFormat data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@SuppressWarnings("deprecation")
	public Map<String, Long> durataTotalaAFiecareiActivitati(){
		return  this.lista.stream().collect(Collectors.groupingBy(x -> x.getActivityLabel(), Collectors.summingLong(x -> {
			
				try {
					return Math.abs((data.parse(x.getEndTimeString()).getMinutes() - data.parse(x.getStartTimeString()).getMinutes()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return 0;
		})));
		
	}
	
	public List<String> nouaZecilaSuta(){

		HashMap<String, Long> conditie5Minute = (HashMap<String, Long>) lista.stream().filter(temp -> {
			if (Math.abs(temp.getEndTime().getMinute() - temp.getStartTime().getMinute()) > 5) {
				return false;
			} else {
				return true;
			}
		}).collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));
		
		return this.lista.stream().filter(x -> {
			if (conditie5Minute.containsKey(x.getActivityLabel())) {
				if (conditie5Minute.get(((MonitoredData) x).getActivityLabel()) > 0.9 * activitatiTotale.get(x.getActivityLabel())) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}).map(y -> {
			return y.getActivityLabel();
		}).distinct().collect(Collectors.toList());
		
	}
}
