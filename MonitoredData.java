
import java.time.*;

public class MonitoredData {
	private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activityLabel;
    
	public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activityLabel) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}
	
	public String getStartTimeString() {
		return "" + startTime.getYear() + "-" + startTime.getMonthValue() + "-" + startTime.getDayOfMonth() + " " + startTime.getHour() + ":" + startTime.getMinute() + ":" + startTime.getSecond();
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
    
	public int getDay() {
		return this.startTime.getDayOfYear();
	}
	
    public String dataString() {
    	return startTime.getYear() + "-" + startTime.getMonthValue() + "-" + startTime.getDayOfMonth();
    }

	public String getEndTimeString() {
		return "" + endTime.getYear() + "-" + endTime.getMonthValue() + "-" + endTime.getDayOfMonth() + " " + endTime.getHour() + ":" + endTime.getMinute() + ":" + endTime.getSecond();
	}
}
