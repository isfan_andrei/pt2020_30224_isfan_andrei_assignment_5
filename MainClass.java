import java.io.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.Map.Entry;

public class MainClass {
	public static void main(String args[]) throws IOException {
		Taskuri T = new Taskuri();

		List<MonitoredData> list = T.creareLista("Activities.txt");
		T.setLista(list);
		scrieInFisier1(T.getLista());
		
		scrieInFisier2(T.totalZile());

		Map<String, Integer> activitati = T.activitatiPeZiDupaNumarulLor();
		T.setActivitati(activitati);
		scrieInFisier3(T.getActivitatiTotale());

		Map<Integer, Map<String, Integer>> activitatiZilnice = T.activitatiInFiecareZiExistenta();
		T.setActivitatiPeZi(activitatiZilnice);
		scrieInFisier4(T.getActivitatiPeZi());
		
		Map<String, Long> activitateDurata = T.durataTotalaAFiecareiActivitati();
		T.setActivitateDurata(activitateDurata);
		scrieInFisier5(T.getActivitateDurata());
		
		scrieInFisier6(T.nouaZecilaSuta());
	}

	public static void scrieInFisier1(List<MonitoredData> lista) {

		File f = new File("Task_1.txt");
		try {
			FileOutputStream fileOut = new FileOutputStream(f);
			PrintWriter printer = new PrintWriter(fileOut);

			for (MonitoredData l : lista) {
				printer.println(l.getStartTime() + "    " + l.getEndTime() + "    " + l.getActivityLabel());
			}

			printer.flush();
			printer.close();
			fileOut.close();
		} catch (Exception e) {
			System.out.println("Eroare!\n");
		}
	}

	public static void scrieInFisier2(int total) {

		File f = new File("Task_2.txt");
		try {
			FileOutputStream fileOut = new FileOutputStream(f);
			PrintWriter printer = new PrintWriter(fileOut);

			printer.println("Avem " + total + " zile\n");

			printer.flush();
			printer.close();
			fileOut.close();
		} catch (Exception e) {
			System.out.println("Eroare!\n");
		}
	}
	
	public static void scrieInFisier3(Map<String, Integer> activitati) {

		File f = new File("Task_3.txt");
		try {
			FileOutputStream fileOut = new FileOutputStream(f);
			PrintWriter printer = new PrintWriter(fileOut);

			for (Map.Entry<String, Integer> i : activitati.entrySet()) {
				printer.println(i.getKey() + " apare de " + i.getValue());
			}
			printer.flush();
			printer.close();
			fileOut.close();
		} catch (Exception e) {
			System.out.println("Eroare!\n");
		}
	}
	
	public static void scrieInFisier4(Map<Integer, Map<String, Integer>> activitatiZilnice) {

		File f = new File("Task_4.txt");
		try {
			FileOutputStream fileOut = new FileOutputStream(f);
			PrintWriter printer = new PrintWriter(fileOut);

			for (Map.Entry<Integer, Map<String, Integer>> i : activitatiZilnice.entrySet()) {
				printer.print("In ziua " + (i.getKey() - 331));
				for (Entry<String, Integer> j : i.getValue().entrySet()) {
					printer.println(" avem " + j.getKey() + " de " + j.getValue() + " ori");
				}
			}
			printer.flush();
			printer.close();
			fileOut.close();
		} catch (Exception e) {
			System.out.println("Eroare!\n");
		}
	}
	
	public static void scrieInFisier5(Map<String, Long> ziuaDurata) {

		File f = new File("Task_5.txt");
		try {
			FileOutputStream fileOut = new FileOutputStream(f);
			PrintWriter printer = new PrintWriter(fileOut);

			for (Entry<String, Long> i : ziuaDurata.entrySet()) {
				printer.print("Activitatea " + i.getKey());
				printer.print(" de durata " + i.getValue() + " minute\n");
			}
			printer.flush();
			printer.close();
			fileOut.close();
		} catch (Exception e) {
			System.out.println("Eroare!\n");
		}
	}
	
	public static void scrieInFisier6(List<String> ziua) {

		File f = new File("Task_6.txt");
		try {
			FileOutputStream fileOut = new FileOutputStream(f);
			PrintWriter printer = new PrintWriter(fileOut);

			for (String i : ziua) {
				printer.print("Activitatea " + i + "\n");
			}
			printer.flush();
			printer.close();
			fileOut.close();
		} catch (Exception e) {
			System.out.println("Eroare!\n");
		}
	}
}
